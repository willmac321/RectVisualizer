# rectVisualizer
JS Rectangle Visualization using p5.js
# RectVisualizer

Inspired by Coding Train's Video @
https://www.youtube.com/watch?v=H81Tdrmz2LA


A cube wave simulation that changes based on the screen size.

![](https://gitlab.com/willmac321/RectVisualizer/raw/master/SquareWave.gif)
